#uses "HVMMGeneral.ctl"

void main()
{

   dpConnect("warningFunctionDewPoint", 
             "System1:BME280.dewPoint.warning:_alert_hdl.._act_state_color"); 
  
   dpConnect("warningFunctionPressure","N1.Gas.Pressure","N2.Gas.Pressure"); 

}

void warningFunctionDewPoint(string dpSource, string sNewColor)
{
 
  if(sNewColor=="informationCamAckn")
  {
         alertViaEmail("christos.paraskevopoulos@cern.ch","dewPointCosmics@cern.ch", "DewPoint Alarm Cosmics@BB5"," KAIGOMASTE, to Low Voltage kai Cooling mikreeeeeeee !!!!! ");
         
         alertViaSMS("0041754111516", "dewPointCosmics@cern.ch", "DewPoint Alarm Cosmics@BB5","Please turn OFF Low Voltage and Cooling, inputFlowTemp-Dewpoint <2.0[C] !!!!!!");
         alertViaSMS("0041754118145", "dewPointCosmics@cern.ch", "DewPoint Alarm Cosmics@BB5","Please turn OFF Low Voltage and Cooling, inputFlowTemp-Dewpoint <2.0[C] !!!!!!");
         alertViaSMS("0041754116441", "dewPointCosmics@cern.ch", "DewPoint Alarm Cosmics@BB5","Please turn OFF Low Voltage and Cooling, inputFlowTemp-Dewpoint <2.0[C] !!!!!!");
         
         alertViaEmail("maria.perganti@cern.ch","dewPointCosmics@cern.ch", "DewPoint Alarm Cosmics@BB5","Please turn OFF Low Voltage and Cooling, inputFlowTemp-Dewpoint <2.0[C] !!!!!!");
         alertViaEmail("kostas.ntekas@cern.ch","dewPointCosmics@cern.ch", "DewPoint Alarm Cosmics@BB5","Please turn OFF Low Voltage and Cooling, inputFlowTemp-Dewpoint <2.0[C] !!!!!!");
         alertViaEmail("theodoros.alexopoulos@cern.ch","dewPointCosmics@cern.ch", "DewPoint Alarm Cosmics@BB5","Please turn OFF Low Voltage and Cooling, inputFlowTemp-Dewpoint <2.0[C] !!!!!!");
         
         alertViaEmail("polyneikis.tzanis@cern.ch","dewPointCosmics@cern.ch", "DewPoint Alarm Cosmics@BB5"," KAIGOMASTE, To Low Voltage kai Cooling mikreeeeeeee !!!!! ");
    
        delay(3600);
   } 
  
  
  
  
}

void warningFunctionPressure(string dpSource1, float newValue1, string dpSource2, float newValue2)
{
 
  if(newValue1>6.0 || newValue2>6.0)
  {
   
         alertViaEmail("christos.paraskevopoulos@cern.ch","pressureCosmics@cern.ch", "Pressure Alarm Cosmics@BB5"," KAIGOMASTE, to pressure mikreeeeeeee !!!!! ");
         
         alertViaSMS("0041754111516", "pressureCosmics@cern.ch", "Pressure Alarm Cosmics@BB5","Warning, pressure > 6.0 [mbar] !!!!!!");
         alertViaSMS("0041754118145", "pressureCosmics@cern.ch", "Pressure Alarm Cosmics@BB5","Warning, pressure > 6.0 [mbar] !!!!!!");
         alertViaSMS("0041754116441", "pressureCosmics@cern.ch", "Pressure Alarm Cosmics@BB5","Warning, pressure > 6.0 [mbar] !!!!!!");
         
         alertViaEmail("maria.perganti@cern.ch","pressureCosmics@cern.ch", "Pressure Alarm Cosmics@BB5","Warning, pressure > 6.0 [mbar] !!!!!!");
         alertViaEmail("kostas.ntekas@cern.ch","pressureCosmics@cern.ch", "Pressure Alarm Cosmics@BB5","Warning, pressure > 6.0 [mbar] !!!!!!");
         alertViaEmail("theodoros.alexopoulos@cern.ch","pressureCosmics@cern.ch", "Pressure Alarm Cosmics@BB5","Warning, pressure > 6.0 [mbar] !!!!!!");  
         
         alertViaEmail("polyneikis.tzanis@cern.ch","pressureCosmics@cern.ch", "Pressure Alarm Cosmics@BB5"," KAIGOMASTE, To pressure mikreeeeeeee !!!!! ");
         
         delay(3600);
   } 
  
  
  
  
}

  
