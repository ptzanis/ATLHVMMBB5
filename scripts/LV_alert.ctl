#uses "HVMMGeneral.ctl"

void main()
{
   dpConnect("setLVOFFdueToArduinoRestart","_ArduinoOPCServer.ServerState");    
   
   
      dyn_string alias;    
  dyn_string allChannels,channelname,channels;
   dyn_string node;
  dyn_dyn_float value;
   
   dyn_dyn_float  mdeg;
   bool isOn;
   
  
   
 allChannels = dpNames("*MDM_*","FwElmbAi");
  for(int i =1; i<=dynlen(allChannels); i++){
   
       dpGet(allChannels[i] +".value",mdeg[i]);
       //mdeg[i]=$temp;
       dpConnect("Alert",allChannels[i] + ".value");
       //DebugN(value);
      
     }
  
}

void setLVOFFdueToArduinoRestart(string dpSource, int valueOPC)
{
 
  bool safetyLight;
  
 if(valueOPC!=1)
  {
    dpSet("analog1.status",1);
    dpSet("analog2.status",1);
    dpSet("analog3.status",1);
    dpSet("digital.status",1);
    
    dpGet("ALL.safetyAlarmLight",safetyLight);
    if(!safetyLight)
    {
      delay(5);
      dpSet("ALL.safetyAlarmLight",FALSE);
    }
      
      
  }
    
  
}
  
  void Alert(string Channeldp,float newValue){ 
   float x,temp;
 string alias = dpGetAlias(Channeldp);
 string recepient;
 
 //DebugN("newValue :"+newValue);
 if (((alias == "IPCL1")||(alias == "IPCL2")||(alias == "IPCR1")||(alias == "IPCR2")||(alias == "HOCL1")||(alias == "HOCL2")||(alias == "HOCR1")||(alias == "HOCR2"))
   && (newValue>=30000) ){
       
    dpSet("analog1.status",1);
    dpSet("analog2.status",1);
    dpSet("analog3.status",1);
    dpSet("digital.status",1);
    
    
         alertViaEmail("christos.paraskevopoulos@cern.ch","lvCosmics@cern.ch", "LV Powered Off-High Temp input","LV Powered Off !!!,Check cooling on Cosmic Stand at "+alias);
         alertViaSMS("0041754111516", "lvCosmics@cern.ch", "LV Powered Off-High Temp input","LV Powered Off !!!,Check cooling on Cosmic Stand at "+alias);
         alertViaSMS("0041754118145", "lvCosmics@cern.ch", "LV Powered Off-High Temp input","LV Powered Off !!!,Check cooling on Cosmic Stand at "+alias);
         alertViaSMS("0041754116441", "lvCosmics@cern.ch", "LV Powered Off-High Temp input","LV Powered Off !!!,Check cooling on Cosmic Stand at "+alias);
         alertViaEmail("kostas.ntekas@cern.ch","lvCosmics@cern.ch", "LV Powered Off-High Temp input","LV Powered Off !!!,Check cooling on Cosmic Stand at "+alias);
         
         alertViaEmail("theodoros.alexopoulos@cern.ch","lvCosmics@cern.ch", "LV Powered Off-High Temp input","LV Powered Off !!!,Check cooling on Cosmic Stand at "+alias);
         alertViaEmail("polyneikis.tzanis@cern.ch","lvCosmics@cern.ch", "LV Powered Off-High Temp input","LV Powered Off !!!,Check cooling on Cosmic Stand at "+alias);
         alertViaEmail("maria.perganti@cern.ch","lvCosmics@cern.ch", "LV Powered Off-High Temp input","LV Powered Off !!!,Check cooling on Cosmic Stand at "+alias);
       
    delay(1800);
       
 }
}

  
  
