#uses "ICS_Test_lib"

/*
  This CB function is called whenever the userDefined.testMe dp is changed.
  Therefore one can run multiple test parallel without them interfering and the possibility to abort every single one of them.
*/



void ics_testBoardCB(string dpThatChanged, bool value)
{
  dyn_errClass err;
  string id;
  string model;
  dyn_string channels;
  string EasyBoard;
  string dpName;
  bool dpValue;
  bool b;
  int replyCode;
  

  dpName = dpSubStr(dpThatChanged, DPSUB_DP); 
  dpGet(dpName + ".userDefined.testMe", dpValue);
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
  

  
  //set variables for the board test
  if(dpValue == TRUE)
  {
    dpGet(dpName + ".userDefined.testVars.id", id);
    dpGet(dpName + ".userDefined.testVars.model", model);
    dpGet(dpName + ".userDefined.testVars.channels", channels);
    dpGet(dpName + ".userDefined.testVars.EasyBoard", EasyBoard);
//     DebugTN(EasyBoard, model);
    
    dpSetWait(EasyBoard + ".userDefined.testStatus", "starting");
    
    ics_testBoard(id, model, channels, EasyBoard);
    
    dpValue = FALSE;
//     DebugTN("online value = " + dpGet(dpName + "userDefined.testMe", b));
    
  }

}    


/////////////////////////////////////////////////////////////////////////////////////////////////
// in this function the test procedure is defined and executed
// the procedure consists of three cycles with the same structure
// 1. steady on
//      all the channels are turned on for 24h 
// 2. even - odd cycle (48 hours)
//      even-odd channels are alternative turned on and off in an hour min rythm (nominal values)
//
//  when the test is finised the program will automatically print an error report
//  the whole procedure will take at least 72 hours
/////////////////////////////////////////////////////////////////////////////////////////////////



ics_testBoard(string id, string model, dyn_string channels, string EasyBoard)
{
  DebugN("ICS lib: " + id  + "\t" + model + "\t" + channels + "\t" + EasyBoard);
  // general variables
  time t = getCurrentTime();
  
  string startTime = formatTime("%X",t);
  float startPeriod = period(t);
  string days = formatTime("%d",t);
  string months = formatTime("%m",t);
  string years = formatTime("%y",t);
  string date = days + "_" + months + "_" + years;
  string fileName;
  string statusDPE;
  string EasyBoardDP = EasyBoard + ".userDefined.testVars.EasyBoard";
  file TestCAENboards;
  time t1 = getCurrentTime();
  
  // old settings (14 Jan 2020)
//   int custom_delay_cycle = 10;  // # cycles ....after each cycle you have the opportunity to abort the test 
//   int custom_delay_second = 1;    // seconds of a cycle
//   
//   int cycle_number = 6;
//   int general_cycle_number =3;
  
  
//    new settings from (17 Jan 2020)
  int custom_delay_cycle = 360;  // # cycles ....after each cycle you have the opportunity to abort the test ,will be edited to 360 cycles
  int custom_delay_second = 10;    // seconds of a cycle , will be edited to 100 seconds
  
  int cycle_number = 24;    // we want 48h test, will be edited to 24 cycles 2 hours each
  int general_cycle_number =1;
 // int general_cycle_number =24;
  
  int replyCode;

  dyn_string VISchannels, Vchannels, Ichannels, Schannels;
  bool testMe;
  dyn_errClass err;
  
  // write status and store some data
  dpSetWait(EasyBoard + ".userDefined.testStatus", "Starting...");
  fileName = model + "_" + id + "_" + date + ".txt";
  DebugN("ICS lib: fileName: " + fileName);
  dpSetWait(EasyBoard + ".userDefined.testVars.date", date);
  dpSetWait(EasyBoard + ".userDefined.testVars.t1", t1);
  dpSetWait(EasyBoard + ".userDefined.testVars.fileName", fileName);
    
  // folder to write data to and prepare file
  TestCAENboards = fopen("C:/TestICSBoards/" + fileName, "a"); //open for writing
  ics_fileHeadings(date, id, model, channels, TestCAENboards); // prints the heading of the output file for this specific module
  fflush(TestCAENboards);

  // prepare array for storing and writing data
  ics_visAppend(EasyBoard);
  DebugTN("Board Dp: ",EasyBoard);

  // switch ICS Board on 
   ics_turnBoardOn(EasyBoard,fileName);
  
//   test circles
  DebugN("ICS lib: Tests will now be started");
  for (int j=1;j<=general_cycle_number;j++)
  {
    // set nominal values  --> if you do't have --> comment out
//     ics_setNominalValues(channels, model, fileName, EasyBoard);
    if (j == 1) {  // display a message
      DebugTN("Beginning test of: board " + id);
      DebugTN("Even - Odd mode:Alternating" + (custom_delay_cycle*custom_delay_second)/60 + " minutes On "+(custom_delay_cycle*custom_delay_second)/60 +" minutes Off for " + cycle_number + " cycles ==> " + (((custom_delay_cycle*custom_delay_second)/60)*cycle_number*2)/60 + " hours");
      dpSetWait(EasyBoard + ".userDefined.testStatus", "1st Test circle");}      
    delay(2);
    
    // ----------
    // switch it on steadily
    DebugN("24h On");
    if (ics_turnOnSteadily(channels, fileName, EasyBoard, model, t1)<0)    {      return;}  // t1 makes sure that 1 h really is over  --> adjust in function!
    
    // ----------
    // swich Even - Odd channels on or off // switch on/off in an hour long interval 
        DebugN("48h Even - Odd cycle"); //DebugN("24h even cycle");
    for(int ii=1; ii<=cycle_number; ii++)
    {
      DebugTN("Even Odd cycle #" +j+ "-"+ ii+ " from " + cycle_number + " cycles");
      if (j == 1) {dpSetWait(EasyBoard + ".userDefined.testStatus", "1st rep. - even odd cycle " + ii);} 
      delay(60);  //60 sec delay  for safety    
      ics_turnChannelsEvenOn(channels, fileName);     //To power On Even channels when Odd are Off  
      if (ics_customDelay(custom_delay_cycle, custom_delay_second, EasyBoard) <0)  //24*2 hour cycles = 48 h, 360 cycles 10s each
        return;
      ics_turnChannelsOff(channels, fileName);
      delay(10);
      ics_turnChannelsOddOn(channels, fileName);  //To power On Odd channels when Even are Off
      DebugTN("Odd cycle #" +j+ "-"+ ii+ " from " + cycle_number + " cycles");
      if (ics_customDelay(custom_delay_cycle, custom_delay_second, EasyBoard) <0)
        return;
       ics_turnChannelsOff(channels, fileName);
    }
    
    // ----------
    // swtich on/off odd channels 
 /*       DebugN("24h odd cycle");
    for(int ii=1; ii<=cycle_number; ii++)
    {
    DebugTN("Odd cycle #" +j+ "-"+ ii + " from " + cycle_number + " cycles");
    if (j == 1) {dpSetWait(EasyBoard + ".userDefined.testStatus", "1st rep. - odd cycle " + ii);}      
      ics_turnChannelsOddOn(channels, fileName);
      if (ics_customDelay(custom_delay_cycle, custom_delay_second, EasyBoard) <0)
        return;      
      ics_turnChannelsOff(channels, fileName);
    //  ics_turnChannelsEvenOn(channels, fileName);  //To power on Even channels when even are off
        if (ics_customDelay(custom_delay_cycle, custom_delay_second, EasyBoard)<0)
          return;
        // ics_turnChannelsOff(channels, fileName);
    } */   
    // ----------
    // switch on complete board for some time
//     if (ics_turnOnSteadily(channels, fileName, EasyBoard, model, t1)<0)
//     {
//       return;
//     }
  }
 
  
  // ----------
  // test procedure done --> write to file
  dpSetWait(EasyBoard + ".userDefined.testStatus", "printing error report");
  
  ics_fileEndings(startPeriod, fileName);
//   ics_setNominalValues(channels, model, fileName, EasyBoard);
  ics_turnChannelsOff(channels, fileName);
  delay(30);
   ics_turnBoardOff(EasyBoard,fileName);
  
  ics_printTemperature(EasyBoard, TestCAENboards);
  fclose(TestCAENboards);
  DebugN("----- END OF FILE " + fileName + "-----");
  
  for (int i=1; i<=dynlen(channels); i++)
  {
    statusDPE = channels[i] + ".Status";    
    replyCode = dpDisconnect("ics_visConnect", statusDPE, EasyBoardDP);
    if( replyCode != 0)
    { 
      DebugTN("ICS_boardTest.ctl: Error = " + replyCode + "Disconnecting actual.status " + statusDPE);
    }
  }
  ics_printErrorReport(fileName, model, id, date);
  
  
  dpSetWait(EasyBoard + ".userDefined.testStatus", "finished - ready to unplug");
  dpSetWait(EasyBoard + "userDefined.testMe", FALSE);
  
}


  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // The following function sets the nominal values as written in the corresponding DP's
  // those are automatically set by the initialize fuction of the main panel or manualy by  
  // the change test parameters function in the main panel.
  //
  // info: changes applied by this panel are not permanent!!!
  //       permanent changes of the voltages and currents should be made in the initialize function of the main panel.
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ics_setNominalValues(dyn_string channels, string model, string fileName, string EasyBoard)
{
  dyn_errClass err;
  float v0;
  float v1;
  float i0;

  
  dpGet(EasyBoard + ".userDefined.nominal.v0", v0);
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
  dpGet(EasyBoard + ".userDefined.nominal.v1", v1);
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
  dpGet(EasyBoard + ".userDefined.nominal.i0", i0);
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
  
      
  for (int c=1; c<=dynlen(channels); c++)
  {
//     DebugTN(channels[c]);
    dpSetWait(channels[c] + ".settings.onOff", 0);
    dpSetWait(channels[c] + ".settings.v0", v0);
    dpSetWait(channels[c] + ".settings.v1", v1);
    dpSetWait(channels[c] + ".settings.i0", i0);
  }
  delay(30);
  DebugTN(model + " set to nominal values");
}



  
  ///////////////////////////////////////////////////////////////////////////////////////////
  // The ics_printErrorReport() function writes a error Report for each channel 
  // of the corresponding board into the output file.
  // As well a conclusive error report is written.
  // The errors are obtained by reading each line of the outputfile in which 
  // data is written (all others do not begin with a number) and checking the 
  // status for each channel. The errors are printed there as given by CAEN
  ///////////////////////////////////////////////////////////////////////////////////////////
  

ics_printErrorReport(string fileName, string model, string id, string date)
{
  string line;
  int numberOfStatusChange, l, i, c, numberOfChannels;
  dyn_string columns, statusString;
  dyn_dyn_int channelStatus;
  dyn_int temperatureErrorN;
  int sumTemperatureErrorN;
  dyn_int powerFailN; 
  int sumPowerFailN;
  dyn_int unpluggedN;
  int sumUnpluggedN;
  dyn_int calibrationErrorN;
  int sumCalibrationErrorN;
  dyn_int internalTripN;
  int sumInternalTripN;
  dyn_int overHVmaxN;
  int sumOverHVmaxN;
  dyn_int underVoltageN;
  int sumUnderVoltageN;
  dyn_int overVoltageN;
  int sumOverVoltageN;
  dyn_int overCurrentN;
  int sumOverCurrentN;

  l = 0;  /// number of lines to be read
  file TestCAENboards = fopen("C:/TestICSBoards/"+fileName, "a+"); //open for reading and writing at the end
  while (feof(TestCAENboards) == 0) 
  {
    fgets(line, 1000, TestCAENboards);
    
    // checking for the right beginning of a line to read just the written data
    if (line[0] != "" && line[0] != " " && line[0] != "#" && line[0] != "\n" && line[0] != "t") // evtl \t?
    {
      l = l + 1;
      columns = strsplit(line, "\t");
      statusString[l] = columns[dynlen(columns)]; //taking just the last coulum of each line (the status)
      numberOfChannels = (dynlen(columns) - 2)/2;
      for( c = 1 ; c <= numberOfChannels; c++)
      {
        temperatureErrorN[c] = 0;
        powerFailN[c] = 0;
        unpluggedN[c] = 0;
        calibrationErrorN[c] = 0;
        internalTripN[c] = 0;
        overHVmaxN[c] = 0;
        underVoltageN[c] = 0;
        overVoltageN[c] = 0;
        overCurrentN[c] = 0;
      }
    } 
  }
  //////CHECK FOR CHANGES IN GENERAL STATUS
  for ( i = 1 ; i <= l ; i++)
  {
    channelStatus[i] = strsplit(statusString[i], "_"); // splits the status string to obtain just the needed nubers
    
    //////CHECK FOR CHANGES IN CHANNEL STATUS
    for( c = 1 ; c <= numberOfChannels ; c++)
    {
      /////CHECK WHAT HAS CHANGED 
      // regarding that all errors for one channel are given by bins converted to decimal numbers
      // which are added, the errors are obtainted by a modulo (%) function and compared to the 
      // error numbers given by CAEN
      ////////
      
      if ( channelStatus[i][c] >= 32768)
        temperatureErrorN[c] = temperatureErrorN[c] +1;
      else {}
      if ( channelStatus[i][c]%32768 >= 16384)
        powerFailN[c] = powerFailN[c] + 1;
      else {}
      if (channelStatus[i][c]%32768%16384 >= 2048)
        unpluggedN[c] = unpluggedN[c] + 1;
      else {}
      if (channelStatus[i][c]%32768%16384%2048 >= 1024)
        calibrationErrorN[c] = calibrationErrorN[c] + 1;
      else {}
      if (channelStatus[i][c]%32768%16384%2048%1024 >= 512)
        internalTripN[c] = internalTripN[c] + 1;
      else {}
      if (channelStatus[i][c]%32768%16384%2048%1024%512 >= 128)
        overHVmaxN[c] = overHVmaxN[c] + 1;
      else {}
      if (channelStatus[i][c]%32768%16384%2048%1024%512%128 >= 32)
        underVoltageN[c] = underVoltageN[c] + 1;
      else {} 
      if (channelStatus[i][c]%32768%16384%2048%1024%512%128%32 >= 16)
        overVoltageN[c] = overVoltageN[c] + 1;
      else {}
      if (channelStatus[i][c]%32768%16384%2048%1024%512%128%32%16 >= 8)
        overCurrentN[c] = overCurrentN[c] + 1;
      else {} 
    }
  }
  
  fprintf(TestCAENboards,"%s \n","########################################");
  fprintf(TestCAENboards,"%s \n","# PROBLEMS THAT SHOWED UP: ");
  fprintf(TestCAENboards,"%s \n","########################################");
  for( c = 1 ; c <= numberOfChannels; c++)
  {
    int v = c-1;
    fprintf(TestCAENboards,"%s \n","########################################");
    fprintf(TestCAENboards,"# CHANNEL: %d \n",v);
    fprintf(TestCAENboards,"# TEMPERATURE ERROR: \t%d \n",temperatureErrorN[c]);
    fprintf(TestCAENboards,"# POWER FAIL: \t\t%d \n",powerFailN[c]);
    fprintf(TestCAENboards,"# UNPLUGGED: \t\t%d \n",unpluggedN[c]);
    fprintf(TestCAENboards,"# CALIBRATION ERROR: \t%d \n",calibrationErrorN[c]);
    fprintf(TestCAENboards,"# INTERNAL TRIP: \t%d \n",internalTripN[c]);
    fprintf(TestCAENboards,"# OVER HVMAX: \t\t%d \n",overHVmaxN[c]);
    fprintf(TestCAENboards,"# UNDER VOLTAGE: \t%d \n",underVoltageN[c]);
    fprintf(TestCAENboards,"# OVER VOLTAGE: \t%d \n",overVoltageN[c]);
    fprintf(TestCAENboards,"# OVER CURRENT: \t%d \n",overCurrentN[c]);
    fprintf(TestCAENboards,"%s \n","########################################");
  }
  fprintf(TestCAENboards,"\n");
  fflush(TestCAENboards);
  fclose(TestCAENboards);

  
  /////////////////////////////////////////////////////////////////////
  // write conclusive error report for each module type
  // as the sum over all channel errors to obtain an overview
  /////////////////////////////////////////////////////////////////////
  
  for( c = 1 ; c <= numberOfChannels ; c++)
  {
    sumTemperatureErrorN+=temperatureErrorN[c];
    sumPowerFailN+=powerFailN[c];
    sumUnpluggedN+=unpluggedN[c];
    sumCalibrationErrorN+=calibrationErrorN[c];
    sumInternalTripN+=internalTripN[c];
    sumOverHVmaxN+=overHVmaxN[c];
    sumUnderVoltageN+=underVoltageN[c];
    sumOverVoltageN+=overVoltageN[c];
    sumOverCurrentN+=overCurrentN[c];
  }
  
  file module = fopen("C:/TestICSBoards/" + model + "Overview.txt", "a");
    fprintf(module,"\n %s", "########################################");
    fprintf(module,"\n %s %s", "###  Obtained errors on board ", id);
    fprintf(module,"\n %s \t %s", "###  On ", date);
    fprintf(module,"\n %s", "########################################");
    fprintf(module,"\n %s \t %d", "# Temperature errors: ", sumTemperatureErrorN);
    fprintf(module,"\n %s \t\t %d", "# Power failures: ", sumPowerFailN);
    fprintf(module,"\n %s \t\t\t %d", "# Unplugged: ", sumUnpluggedN);
    fprintf(module,"\n %s \t %d", "# Calibration Errors: ", sumCalibrationErrorN);
    fprintf(module,"\n %s \t\t %d", "# Internal Trips: ", sumInternalTripN);
    fprintf(module,"\n %s \t\t %d", "# Over max HV: ", sumOverHVmaxN);
    fprintf(module,"\n %s \t\t %d", "# Voltage too low: ", sumUnderVoltageN);
    fprintf(module,"\n %s \t\t %d", "# Voltage too high: ", sumOverVoltageN);
    fprintf(module,"\n %s \t\t %d", "# Current too high: ", sumOverCurrentN);    
    fprintf(module,"\n %s", "######################################## \n \n \n");
    
  fflush(module);  
  fclose(module);

  
}

int ics_turnOnSteadily(dyn_string channels, string fileName, string EasyBoard, string model, time t1)
{
  bool testMe;
  dyn_errClass err;
      
//   ics_setNominalValues(channels, model, fileName, EasyBoard);
  //DebugTN(fileName + " Turned on steadily for 1 hour");
  DebugTN(fileName + " Turned on steadily for 24 hour");
   file TestCAENboards = fopen("C:/TestICSBoards/"+fileName, "a+");
  // fprintf(TestCAENboards,"\n%s \n","#####################  steadily on for 1h  ###################");
    fprintf(TestCAENboards,"\n%s \n","#####################  steadily on for 24h  ###################");
   
  ics_turnChannelsOn(channels, fileName);

 // dpSetWait(EasyBoard + ".userDefined.testStatus", "Steady on 1h");  
  dpSetWait(EasyBoard + ".userDefined.testStatus", "Steady on 24h"); 
  
  
  // should be delay(3600);
  if (ics_customDelay(8640, 10, EasyBoard) <0)
  //   if (ics_customDelay(8, 5, EasyBoard) <0) // delay test setting
    return(-1);

 
ics_turnChannelsOff(channels, fileName);
//dpSetWait(EasyBoard + ".userDefined.testStatus", "Off 5min");

// should be: delay(300);
//       if (ics_customDelay(60, 5, EasyBoard) <0)    // equals 1 h
   //   if (ics_customDelay(8640, 10, EasyBoard) <0)    // equals 24 h
      //  return(-1);
      
  return(0);
}


ics_turnChannelsEvenOn(dyn_string channels, string fileName)
{
  for (int ii=1; ii<=dynlen(channels); ii++)
  {
    if((ii-1)%2==0){
      dpSetWait(channels[ii] + ".onOff",1);
    }
    else{
       dpSetWait(channels[ii] + ".onOff",0);
    }
  }
  DebugN(fileName + " board turned on");
}

ics_turnChannelsOddOn(dyn_string channels, string fileName)
{
  for (int ii=1; ii<=dynlen(channels); ii++)
  {
    if((ii-1)%2==0){
       dpSetWait(channels[ii] + ".onOff",0);
    }
    else{
       dpSetWait(channels[ii] + ".onOff",1);
    }
  }
  DebugN(fileName + " board turned on");
}

// switch the channels of 1 ICS Board on
ics_turnChannelsOn(dyn_string channels, string fileName)
{
  for (int ii=1; ii<=dynlen(channels); ii++)
  {
   dpSetWait(channels[ii] + ".onOff",1);
  }
  DebugTN(fileName + " board turned on");
}

// switch the channels of 1 ICS Board off
ics_turnChannelsOff(dyn_string channels, string fileName)
{
  for (int ii=1; ii<=dynlen(channels); ii++)
  {
     dpSetWait(channels[ii] + ".onOff",0);
  }
  DebugN(fileName + " board turned off");
}
 
// switch the ICS Board on
ics_turnBoardOn(dyn_string EasyBoard, string fileName)
{
  for (int ii=1; ii<=dynlen(EasyBoard); ii++)
  {//"dist_1:ICS_A_1.onOff"
    dpSetWait( EasyBoard+ ".onOff",1);
  }
  DebugTN(fileName + " board turned on");
}

// switch the ICS Board off
ics_turnBoardOff(dyn_string EasyBoard, string fileName)
{
  for (int ii=1; ii<=dynlen(EasyBoard); ii++)
  {
    dpSetWait(EasyBoard + ".onOff",0);
  }
 
  DebugN(fileName + " board turned off");
}

ics_fileHeadings( string date, string id, string model, dyn_string channels, file TestCAENboards)
{
  time stime = getCurrentTime();
  string startTime = formatTime("%X",stime);
  float startPeriod = period(stime);
 
  fprintf(TestCAENboards,"%s \n","########################################");
  fprintf(TestCAENboards,"%s \n","# OUTPUT FILE OF CAEN EASY BOARDS TEST  ");
  fprintf(TestCAENboards,"%s \n","########################################");
  fprintf(TestCAENboards,"# date of test: %s \n",date);
  fprintf(TestCAENboards,"# test starting time: %s \n",startTime);
  fprintf(TestCAENboards,"# id of tested easy board: %s \n",id);
  fprintf(TestCAENboards,"# model of tested easy board: %s \n", model);
  fprintf(TestCAENboards,"%s\n\n\n","########################################\n");
  fflush(TestCAENboards);
  
 // Header line for table
    fprintf(TestCAENboards, "%s         ", "time");
  
     // add 8 columns for V 
      for(int jj=1; jj<=8; jj++){
        fprintf(TestCAENboards,"%s%d\t%s%d\t%s%d\t%s%d%s%d\t","V_Ch0",jj,
                                         "I_Ch0",jj,
                                         "St_Ch0",jj,
                                         "OnOf_Ch0",jj,
                                         "T_Ch0",jj);
        
      }
    // add 8 columns vor I
  //  for(int jj=1; jj<=8; jj++)
   //   fprintf(TestCAENboards, "\t%s%d     ","I_Ch0", jj);
    //for(int jj=1; jj<=8; jj++)
     // fprintf(TestCAENboards, "\t%s%d     ","St_Ch0", jj);
    //for(int jj=1; jj<=8; jj++)
      //fprintf(TestCAENboards, "\t%s%d     ","OnOff_Ch0", jj);
    //for(int jj=1; jj<=8; jj++)
     // fprintf(TestCAENboards, "\t%s%d     ","T_Ch0", jj);}
      
      // names of ICS Board specific paramteres
     fprintf(TestCAENboards,"\t%s\t%s\t%s\t%s\t","tempBoardHVmon",
                                            "tempBoardVcc",
                                            "tempBoardISMon",
                                            "tempBoardVSMon");
    
    fprintf(TestCAENboards, "\n\n\n\n");
  // dump to file
  fflush(TestCAENboards);
}


ics_fileEndings( float startPeriod, string fileName)
{
  time etime = getCurrentTime();
  string endTime = formatTime("%X",etime);
  float endPeriod = period(etime);
  float duration;
  file TestCAENboards = fopen("C:/TestICSBoards/" + fileName, "a+");

  duration = (endPeriod - startPeriod)/3600.;
  fprintf(TestCAENboards,"\n\n\n%s \n","########################################");
  fprintf(TestCAENboards,"# test ending time: %s \n",endTime);
  fprintf(TestCAENboards,"# time required for the test: %f hours\n",duration);
  fprintf(TestCAENboards,"%s \n\n\n","########################################");
  fflush(TestCAENboards);
  fclose(TestCAENboards);
}



/////////////////////////////////////////////////////////////////////////////////////////////////
// The ics_printValues() function prints the voltage, current and status into the outputfile
// The file is located in: C:/TestICSBoards/filename
// filename = model + "_" + id + "_" + date + ".txt"
/////////////////////////////////////////////////////////////////////////////////////////////////

ics_printValues(dyn_float VISchannels, time t1, string fileName)  
{
  time t2 = getCurrentTime();
  float t3 = t2-t1; // time between start of test and now

  dyn_int StatusIDs;
  dyn_float Vchannels, Ichannels, Schannels, Tchannels;
  dyn_bool OOchannels;
  dyn_float tempBoardVccs, tempBoardHVMons, tempBoardISMons , tempBoardVSMons;
  dyn_bool tempBoardOOs;
  
  int  ChanNum = 5; //  dynlen(VISchannels)/5;
  dyn_string VchannelsStr, IchannelsStr, SchannelsStr, OOchannelsStr, TchannelsStr ; 
  string tempBoardVccsStr, tempBoardHVMonsStr, tempBoardISMonsStr , tempBoardVSMonsStr,tempBoardOOsStr;
  string status;
  file TestCAENboards = fopen("C:/TestICSBoards/" + fileName, "a+");
  
//   DebugN(VISchannels);

  for (int ii=1; ii<=8; ii++)
  {
    dynAppend(Vchannels, VISchannels[1 + (ii-1) * ChanNum]);  
    dynAppend(Ichannels, VISchannels[2 + (ii-1) * ChanNum]);
    dynAppend(Schannels, VISchannels[3 + (ii-1) * ChanNum]);
    dynAppend(OOchannels, VISchannels[4 + (ii-1) * ChanNum]);
    dynAppend(Tchannels, VISchannels[5 + (ii-1) * ChanNum]);
//     DebugN("ch: " + ii + " V: " + VISchannels[1 + (ii-1) * ChanNum] 
//            + " I: " + VISchannels[2 + (ii-1) * ChanNum]
//            + " S: " + VISchannels[3 + (ii-1) * ChanNum]
//            + " OO: " + VISchannels[4 + (ii-1) * ChanNum]
//            + " T: " + VISchannels[5 + (ii-1) * ChanNum]);

    sprintf(VchannelsStr[ii], "%5.2f", Vchannels[ii]);
    sprintf(IchannelsStr[ii], "%5.1f", Ichannels[ii]);
    sprintf(SchannelsStr[ii], "%d", Schannels[ii]);
    sprintf(OOchannelsStr[ii], "%d", OOchannels[ii]);
    sprintf(TchannelsStr[ii], "%d", Tchannels[ii]);
   }

//     dynAppend(tempBoardVccs, VISchannels[41]);  
//     dynAppend(tempBoardHVMons, VISchannels[42]);
//     dynAppend(tempBoardISMons, VISchannels[43]);
//     dynAppend(tempBoardVSMons, VISchannels[44]);
//     dynAppend(tempBoardOOs, VISchannels[45]);
    sprintf(tempBoardVccsStr, "%5.1f",VISchannels[41]); 
    sprintf(tempBoardHVMonsStr, "%5.1f",VISchannels[42]);
    sprintf(tempBoardISMonsStr, "%5.1f",VISchannels[43]);
    sprintf(tempBoardVSMonsStr, "%5.1f",VISchannels[44]);
    sprintf(tempBoardOOsStr, "%5.1f",VISchannels[45]);


    fprintf(TestCAENboards, "\n%f ", t3);
    // I removed the different for loops
    // intentionally it was written all V (8x) values, then (8x) I values and so on
   // now it is  V, I , S , OO, T
    for(int j=1; j<=dynlen(VchannelsStr); j++)
    {
      fprintf(TestCAENboards, "\t %s", VchannelsStr[j]);
//     for(int j=1; j<=dynlen(IchannelsStr); j++)
      fprintf(TestCAENboards, "\t %s", IchannelsStr[j]);
    fprintf(TestCAENboards, "\t");
//     for(int j=1; j<=dynlen(SchannelsStr); j++)
      fprintf(TestCAENboards, "\t %s", SchannelsStr[j]/* +"_"*/);
//     fprintf(TestCAENboards,"\t"/* SchannelsStr[dynlen(SchannelsStr)]*/);
//     for(int j=1; j<=dynlen(OOchannelsStr); j++)
      fprintf(TestCAENboards, "\t %s", OOchannelsStr[j]);
//     fprintf(TestCAENboards, "\t");
//     for(int j=1; j<=dynlen(TchannelsStr); j++)
//       {
      fprintf(TestCAENboards, "\t %s", TchannelsStr[j]);
    }
    
        
     fprintf(TestCAENboards, "\t %s", tempBoardVccsStr);
     fprintf(TestCAENboards, "\t %s", tempBoardHVMonsStr);
     fprintf(TestCAENboards, "\t %s", tempBoardISMonsStr);
     fprintf(TestCAENboards, "\t %s", tempBoardVSMonsStr);
     fprintf(TestCAENboards, "\t %s", tempBoardOOsStr);
     fprintf(TestCAENboards, "\t %s", "line end");
  
  
  
  fflush(TestCAENboards);
  fclose(TestCAENboards);
}

ics_printTemperature(string channels, file TestCAENboards)   //wrong for the ICS must be edited
{
  float temp;
  dyn_errClass err;
//
  dpGet(board + ".Temp", temp);
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
  fprintf(TestCAENboards, "%s \n","########################################");
  fprintf(TestCAENboards, "#temperature of the board: %f \n", temp);
  fprintf(TestCAENboards, "%s \n\n\n","########################################");
  fflush(TestCAENboards);  
}



/////////////////////////////////////////////////////////////////////////////////////////////////
// the ics_visConnect function is needed to call the ics_visAppend() whenever a channel status changed
/////////////////////////////////////////////////////////////////////////////////////////////////

ics_visConnect(string statusDPE, int statusDPEint, string EasyBoardDP, string EasyBoard)
{
  ics_visAppend(EasyBoard);
}



/////////////////////////////////////////////////////////////////////////////////////////////////
// the visAppend function obtains the actual current and voltage for each channel from the
// board and prints them into the file via the printValues function
/////////////////////////////////////////////////////////////////////////////////////////////////

ics_visAppend(string EasyBoard)
{  
  dyn_string tempV, tempI, tempS,  tempOO, tempT;
  dyn_bool tempOO;
  dyn_string tempBoardVcc, tempBoardHVMon, tempBoardISMon, tempBoardOO, tempBoardVSMon;
  time t1;
  string fileName;
  dyn_string VISchannels, Vchannels, Ichannels, Schannels, Tchannels;
  dyn_bool OOchannels;
  dyn_string tempBoardVccs, tempBoardHVMons, tempBoardISMons, tempBoardOOs, tempBoardVSMons;
  dyn_string channels;
  dyn_errClass err;
  channels = dpNames(EasyBoard + "*","ICS_Channel");
  
  dpGet(EasyBoard + ".userDefined.testVars.channels", channels);
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
    
  dpGet(EasyBoard + ".userDefined.testVars.t1", t1);
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
    
  dpGet(EasyBoard + ".userDefined.testVars.fileName", fileName);
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
  
//   DebugTN(channels, fileName);
    
//   DebugTN("............VIS append.................");

    
    // channel specific 
  for(int ii=1; ii<=dynlen(channels); ii++)
   {
    dpGet(channels[ii] + ".VMon",tempV); // tempV = temporary V
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
     dynAppend(Vchannels, tempV);
   //  DebugTN(Vchannels[ii]+" & tempV "+tempV);
    
    dpGet(channels[ii] + ".IMon",tempI);
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
     dynAppend(Ichannels, tempI);
//      DebugTN(Ichannels[ii]);
    
    dpGet(channels[ii] + ".Status",tempS);
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
     dynAppend(Schannels, tempS);
     
     dpGet(channels[ii] + ".onOff",tempOO);
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
     dynAppend(OOchannels, tempOO);
     
     dpGet(channels[ii] + ".Temp",tempT);
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
     dynAppend(Tchannels, tempT);
//      DebugTN(Schannels[ii]);
   dynAppend(VISchannels, Vchannels[ii]);
   dynAppend(VISchannels, Ichannels[ii]);
   dynAppend(VISchannels, Schannels[ii]);
   dynAppend(VISchannels, OOchannels[ii]);
   dynAppend(VISchannels, Tchannels[ii]);
   }
    
     // Board specific
     dpGet(EasyBoard + ".HVMon",tempBoardHVMon); // tempV = temporary V
     err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
     dynAppend(tempBoardHVMons, tempBoardHVMon);
     
     dpGet(EasyBoard + ".VCCMon",tempBoardVcc); // 
     err=getLastError();
     if(dynlen(err) >0 ) errorDialog(err);
     dynAppend(tempBoardVccs, tempBoardVcc);
    
     dpGet(EasyBoard + ".ISMon",tempBoardISMon); 
     err=getLastError();
     if(dynlen(err) >0 ) errorDialog(err);
     dynAppend(tempBoardISMons, tempBoardISMon);
    
     dpGet(EasyBoard + ".VSMon",tempBoardVSMon); 
     err=getLastError();
     if(dynlen(err) >0 ) errorDialog(err);
     dynAppend(tempBoardVSMons, tempBoardVSMon);
     
     // last columns of output file are ICS Board specific
   dynAppend(VISchannels, tempBoardVccs);
  // DebugN("Board VCC : "+tempBoardVccs);
   dynAppend(VISchannels, tempBoardHVMons);
   dynAppend(VISchannels, tempBoardISMons);
   dynAppend(VISchannels, tempBoardOOs);
   dynAppend(VISchannels, tempBoardVSMons);
     
     // firstst columns of output file reply to the channels of the ICS Board
   dynAppend(VISchannels, Vchannels);
   dynAppend(VISchannels, Ichannels);
   dynAppend(VISchannels, Schannels);
   dynAppend(VISchannels, OOchannels);
   dynAppend(VISchannels, Tchannels);
   
   
   
   ics_printValues(VISchannels, t1, fileName); // this function will print the values into the output file
}




/////////////////////////////////////////////////////////////////////////////////////////////////
// the customDelay function is needed to give the operator the possibility
// to abort a test at every point in the procedure.
// The variable cycles sets the number of repeats in this function
// The variable del sets the frequency in which the cycles are passed and may be aborted
//
// This function is crutial for the operation of the system regarding that it checks for tripping 
// channels and turns them on again.
//
//////////////////////////////////////////////////////////////////////////////////////////////////


int ics_customDelay(int cycles, int del, string EasyBoard)
{
    dyn_string channels;
    string model;
    string date;
    string id;
    bool testMe;
    bool trip;
    dyn_errClass err;
    anytype replyCode;
    time t1;
    string fileName;
   // string EasyBoardDP = EasyBoard + ".userDefined.testVars.EasyBoard";
    string EasyBoardDP = EasyBoard + ".userDefined.testVars.EasyBoard";
    
        
  dpGet(EasyBoard + ".userDefined.testVars.t1", t1);
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
    
  dpGet(EasyBoard + ".userDefined.testVars.fileName", fileName);
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
    
  dpGet(EasyBoard + ".userDefined.testVars.model", model);   
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
    
  dpGet(EasyBoard + ".userDefined.testVars.date", date);   
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err); 
    
  dpGet(EasyBoard + ".userDefined.testVars.id", id);   
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err); 
    
  dpGet(EasyBoard + ".userDefined.testVars.channels", channels);
    err=getLastError();
    if(dynlen(err) >0 ) errorDialog(err);
    
  for (int j=1; j<=cycles; j++) //test
      {
      ics_visAppend(EasyBoard);
      delay(del);
        dpGet(EasyBoard + ".userDefined.testMe",testMe);
            err=getLastError();
            if(dynlen(err) >0 ) errorDialog(err);
        
        if (testMe == FALSE) // note to future me (Paul): abbruch per dpConnect 
          
        {
          dpSetWait(EasyBoard + ".userDefined.testStatus", "aborting...");
          AbortBUTTON.enabled(0);
//           ics_setNominalValues(channels, model, fileName, EasyBoard);
//           setSafeVoltage(channels, model, fileName, EasyBoard);
            file TestCAENboards = fopen("C:/TestICSBoards/"+fileName,"a"); // open for writing
            fprintf(TestCAENboards,"\n%s \n", "#########################################################################");
            fprintf(TestCAENboards,"%s \n", "###  Test Aborted");
            fprintf(TestCAENboards,"%s \n \n \n", "#########################################################################");
            fclose(TestCAENboards);
          DebugTN("Board " + model + " - " +  " set to save voltage");
        
          ics_turnChannelsOff(channels, fileName);
            for (int i=1; i<=dynlen(channels); i++)
            {
            dyn_string split=strsplit(channels[i],":");
              string statusDPE = split[2] + ".Status"; 
           DebugN(" ICS ICS ICS  status DPE : "+statusDPE+"  ICS ICS ICS EASYBOARdDP : " +EasyBoardDP);   
              replyCode = dpDisconnect("ics_visConnect", statusDPE, EasyBoardDP);
                if( replyCode != 0)
                { 
                DebugTN("ICS_Test_lib.ctl: Error = " + replyCode + "Disconnecting actual.status " + statusDPE);
                 err=getLastError();
                 if(dynlen(err) >0 ) errorDialog(err);
                }
            }

          
          ics_printErrorReport(fileName, model, id, date);
          dpSetWait(EasyBoard + ".userDefined.testStatus", "aborted - ready to unplug");
          return(-1);
        }
      }
    return(0);
}
